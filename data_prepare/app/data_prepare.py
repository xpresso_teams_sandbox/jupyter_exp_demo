MOUNT_PATH = None
import os
import sys
import pickle
import urllib
import sklearn
import numpy as np
import pandas as pd
import sklearn.linear_model
import matplotlib.pyplot as plt
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

try:
    oecd_bli = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/oecd_bli.pkl", "rb" ))
    gdp_per_capita = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/gdp_per_capita.pkl", "rb" ))
    X = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/X.pkl", "rb" ))
    Y = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/Y.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = jupyter_exp_demo
## $xprparam_componentname = data_prepare
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = ["oecd_bli", "gdp_per_capita", "X", "Y"]
## $xprjobparam_learningrate = 0.4

"""
This is the implementation of the Data prepare component.

This component prepares data according to the model requirements
"""


oecd_bli = None
gdp_per_capita = None
X = None
Y = None


DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"


class DataPrepareComponent():
    
    def __init__(self, datapath='datasets/lifesat/', root_dir="."):
        self.datapath = datapath
        self.root_dir = root_dir
        
    @staticmethod
    def prepare_country_stats(oecd_bli, gdp_per_capita):
        oecd_bli = oecd_bli[oecd_bli["INEQUALITY"]=="TOT"]
        oecd_bli = oecd_bli.pivot(index="Country", columns="Indicator", values="Value")
        gdp_per_capita.rename(columns={"2015": "GDP per capita"}, inplace=True)
        gdp_per_capita_temp = gdp_per_capita.set_index("Country", inplace=False)
        full_country_stats = pd.merge(left=oecd_bli, right=gdp_per_capita_temp,
                                      left_index=True, right_index=True)
        full_country_stats.sort_values(by="GDP per capita", inplace=True)
        remove_indices = [0, 1, 6, 8, 33, 34, 35]
        keep_indices = list(set(range(36)) - set(remove_indices))
        return full_country_stats[["GDP per capita", 'Life satisfaction']].iloc[keep_indices]
    
    def download_data_files_locally(self):
        os.makedirs(self.datapath, exist_ok=True)
        for filename in ("oecd_bli_2015.csv", "gdp_per_capita.csv"):
            print("Downloading", filename)
            url = DOWNLOAD_ROOT + "datasets/lifesat/" + filename
            urllib.request.urlretrieve(url, self.datapath + filename)

    def load_data(self):
        oecd_bli = pd.read_csv(self.datapath + "oecd_bli_2015.csv", thousands=',')
        gdp_per_capita = pd.read_csv(self.datapath + "gdp_per_capita.csv",thousands=',',delimiter='\t',
                                     encoding='latin1', na_values="n/a")
        return oecd_bli, gdp_per_capita
    
    def prepare_data(self, oecd_bli, gdp_per_capita):
        country_stats = self.prepare_country_stats(oecd_bli, gdp_per_capita)
        X = np.c_[country_stats["GDP per capita"]]
        Y = np.c_[country_stats["Life satisfaction"]]
        return X, Y

# ------------ Code for Local use-------------
def main():
    dp = DataPrepareComponent()
    
    # Only to be used when running locally
    dp.download_data_files_locally()
    
    # Prepare data component
    oecd_bli, gdp_per_capita = dp.load_data()
    X, Y = dp.prepare_data(oecd_bli, gdp_per_capita)
    
    return oecd_bli, gdp_per_capita, X, Y

if __name__ == "__main__":
    oecd_bli, gdp_per_capita, X, Y = main()

try:
    pickle.dump(oecd_bli, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/oecd_bli.pkl", "wb"))
    pickle.dump(gdp_per_capita, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/gdp_per_capita.pkl", "wb"))
    pickle.dump(X, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/X.pkl", "wb"))
    pickle.dump(Y, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/Y.pkl", "wb"))
except Exception as e:
    print(str(e))
