MOUNT_PATH = None
import os
import sys
import pickle
import urllib
import sklearn
import numpy as np
import pandas as pd
import sklearn.linear_model
import matplotlib.pyplot as plt
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

try:
    None = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/None.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = jupyter_exp_demo
## $xprparam_componentname = data_visualize
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = [None]
## $xprjobparam_learningrate = 0.4

"""
This is the implementation of the PlotSave component.

This component can be used to visualize data and 
then save the plots on NFS.
"""
    
class PlotSaveComponent():

    def __init__(self, plot=plt, datapath='datasets/lifesat/', root_dir="."):
        self.datapath = datapath
        self.root_dir = root_dir
        self.plt = plot
        
    def save_fig(self, fig_id, tight_layout=True, fig_extension="png", resolution=300):
        image_path = os.path.join(self.root_dir, "images")
        os.makedirs(image_path, exist_ok=True)
        path = os.path.join(image_path, fig_id + "." + fig_extension)
        print("Saving figure", fig_id)
        if tight_layout:
            self.plt.tight_layout()
        self.plt.savefig(path, format=fig_extension, dpi=resolution)


try:
    pickle.dump(None, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/None.pkl", "wb"))
except Exception as e:
    print(str(e))
