from xpresso.ai.core.commons.network.http.http_request import *
from xpresso.ai.core.commons.network.http.http_request_handler \
    import HTTPHandler
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    HTTPRequestFailedException, HTTPInvalidRequestException, \
    ControllerClientResponseException
from xpresso.ai.core.commons.utils import error_codes
from json import JSONDecodeError
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger

# TODO: Update the class so that ControllerClientResponseExcecption
#  is handled correctly


class SendHTTPRequest:
    """
    sends http request
    """
    API_JSON_OUTCOME = "outcome"
    API_JSON_RESULTS = "results"
    API_JSON_ERROR_CODE = "error_code"
    API_JSON_SUCCESS = "success"
    API_JSON_FAILURE = "failure"

    def __init__(self):
        self.logger = XprLogger()

    def send(self, url: str, http_method: HTTPMethod, data=None, header=None):
        request = HTTPRequest(method=http_method, url=url, headers=header,
                              data=data)
        handler = HTTPHandler()
        try:
            response = handler.send_request(request)
            json_response = response.get_data_as_json()
            if not json_response:
                raise ControllerClientResponseException(
                    "Request Failed", error_codes.server_error)
            elif (json_response[self.API_JSON_OUTCOME] == self.API_JSON_SUCCESS
                  and self.API_JSON_RESULTS in json_response):
                return json_response[self.API_JSON_RESULTS]
            elif (json_response[self.API_JSON_OUTCOME] == self.API_JSON_SUCCESS
                  and self.API_JSON_RESULTS not in json_response):
                return {}
            elif (self.API_JSON_RESULTS in json_response
                  and self.API_JSON_ERROR_CODE in json_response):
                raise ControllerClientResponseException(
                    json_response[self.API_JSON_RESULTS],
                    json_response[self.API_JSON_ERROR_CODE])
            elif self.API_JSON_ERROR_CODE in json_response:
                raise ControllerClientResponseException(
                    "Request Failed", json_response[self.API_JSON_ERROR_CODE])
            raise ControllerClientResponseException(
                "Request Failed", -1)
        except HTTPRequestFailedException as e:
            self.logger.error(str(e))
            raise ControllerClientResponseException(
                "Server is not accessible", error_codes.server_error)
        except HTTPInvalidRequestException as e:
            self.logger.error(str(e))
            raise ControllerClientResponseException(
                "Unable to send request to api_server", error_codes.server_error)
        except JSONDecodeError as e:
            self.logger.error(str(e))
            raise ControllerClientResponseException(
                "Invalid response from api_server", error_codes.server_error)
