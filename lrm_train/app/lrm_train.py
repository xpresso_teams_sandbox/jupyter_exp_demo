MOUNT_PATH = None
import os
import sys
import pickle
import urllib
import sklearn
import numpy as np
import pandas as pd
import sklearn.linear_model
import matplotlib.pyplot as plt
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

try:
    lr_model = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/lr_model.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = jupyter_exp_demo
## $xprparam_componentname = lrm_train
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = ["lr_model"]
## $xprjobparam_learningrate = 0.6


"""
This is the implementation of training linear regression model for 
oecd life satisfaction data v/s gdp per capita in a country.

This component saves a model.
"""

__author__ = "Shlok Mohan Chaudhari"


lr_model = None


class LRM_Training():

    def __init__(self, datapath='datasets/lifesat/', root_dir="."):
        self.datapath = datapath
        self.root_dir = root_dir
    
    @staticmethod
    def lr_train(X, Y):
        lr_model = sklearn.linear_model.LinearRegression()
        lr_model.fit(X, Y)
        return lr_model

# ------------ Code for Local use-------------
def main():

    # Visualize component
    dp = DataPrepareComponent()
    country_stats = dp.prepare_country_stats(oecd_bli, gdp_per_capita)
    country_stats.plot(kind='scatter', x="GDP per capita", y='Life satisfaction')
    plt.show()
    PlotSaveComponent(plot=plt).save_fig("scatter")

    # Train component
    lr_model = LRM_Training().lr_train(X, Y)

    return lr_model


if __name__ == "__main__":
    lr_model = main()# Infer component
X_new = [[22587], [23423]]  # Cyprus' GDP per capita
print("{} is the Life satisfaction pointer for a GDP per capita of {}".format(lr_model.predict(X_new), X_new))

try:
    pickle.dump(lr_model, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/lr_model.pkl", "wb"))
except Exception as e:
    print(str(e))
