MOUNT_PATH = None
import os
import sys
import pickle
import urllib
import sklearn
import numpy as np
import pandas as pd
import sklearn.linear_model
import matplotlib.pyplot as plt
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

try:
    None = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/None.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = jupyter_exp_demo
## $xprparam_componentname = data_fetch
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = [None]
## $xprjobparam_learningrate = 0.4

"""
This is the implementation of the Data fetch component.
It internally uses the xpresso.ai marketplace data_fetch which
migrates data from data versioning system to the NFS mount_path
"""
    
class DataFetchComponent():

    def __init__(self):
        pass

if __name__ == "__main__":
    print('main section')

try:
    pickle.dump(None, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_demo/pickles/None.pkl", "wb"))
except Exception as e:
    print(str(e))
